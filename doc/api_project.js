define({
  "name": "星瀚",
  "version": "1.0.0",
  "description": "星瀚接口文档-接口地址 http://sky.nnzhp.cn",
  "title": "星瀚接口文档",
  "sampleUrl": "http://sky.nnzhp.cn",
  "template": {
    "aloneDisplay": false,
    "jQueryAjaxSetup": {
      "headers": {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST,GET,PUT,DELETE"
      }
    }
  },
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-09-01T17:33:11.370Z",
    "url": "https://apidocjs.com",
    "version": "0.29.0"
  }
});
