import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import _const_var from "./utils/const";
import tools from "./utils/tools"
import VueClipboard from 'vue-clipboard2'
// import MintUI from 'mint-ui'
// import 'mint-ui/lib/style.css'
import Vant from 'vant';
import 'vant/lib/index.css';

Vue.config.productionTip = false
Vue.prototype.$const_var = _const_var //添加全局变量，可以在vue组件里面直接使用，不需要import了
Vue.prototype.$tools = tools  //添加全局函数，可以在vue组件里面直接使用，不需要import了
Vue.use(ElementUI)
Vue.use(VueClipboard)
// Vue.use(MintUI)
Vue.use(Vant);




new Vue({
    render: h => h(App),
    router,
}).$mount('#app')
