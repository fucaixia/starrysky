export default {
    title: "星瀚",
    page_size: 20,
    page_range: [20, 50, 200, 400],
    mock_request_methods: {"GET": 0, "POST": 1, "PUT": 2, "DELETE": 3},
    rsa_type: [{name: "加密", id: 1}, {name: "解密", id: 2}],
    bank_list: [
        {"code": "ABC", "name": "中国农业银行"},
        {"code": "CEB", "name": "中国光大银行"},
        {"code": "CITIC", "name": "中信银行"},
        {"code": "SHBANK", "name": "上海银行"},
        {"code": "SPDB", "name": "上海浦东发展银行"},
        {"code": "CMB", "name": "招商银行"},
        {"code": "ICBC", "name": "中国工商银行"},
        {"code": "BOC", "name": "中国银行"},
        {"code": "CCB", "name": "中国建设银行"},
        {"code": "PSBC", "name": "中国邮政储蓄银行"},
        {"code": "QHDBANK", "name": "秦皇岛银行"},
        {"code": "SPABANK", "name": "平安银行"},
        ],
    "bank_card_type":[
        {code:"CC",name:"借记卡"},
        {code:"DC",name:"信用卡"},
        ]
}