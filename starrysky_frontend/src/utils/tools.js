var token_key = "token"
var roles_key = "roles"
var phone_key = "phone"
var user_id_key = "user_id"
var email_key = "email"
var user_nick_key = "nick"
export default {
    get_token() {
        return localStorage.getItem(token_key)
    },
    set_token(token) {
        localStorage.setItem(token_key, token)
    },
    remove_token() {
        localStorage.removeItem(token_key)
    },
    set_user_info(data) {
        localStorage.setItem(roles_key, JSON.stringify(data.roles))
        localStorage.setItem(user_id_key, data.id)
        localStorage.setItem(email_key, data.email)
        localStorage.setItem(phone_key, data.phone)
        localStorage.setItem(user_nick_key, data.nick)
    },
    delete_user_info() {
        localStorage.removeItem(roles_key)
        localStorage.removeItem(user_id_key)
        localStorage.removeItem(email_key)
        localStorage.removeItem(phone_key)
        localStorage.removeItem(user_nick_key)
    },
    get_roles() {
        return JSON.parse(localStorage.getItem(roles_key))
    },

    get_user_id() {
        return localStorage.getItem(user_id_key)
    },
    get_email() {
        return localStorage.getItem(email_key)
    },
    get_phone() {
        return localStorage.getItem(phone_key)
    },
    get_nick() {
        return localStorage.getItem(user_nick_key)
    }
}



