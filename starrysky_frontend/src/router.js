import Vue from "vue";
import encrypt_decrypt from './views/qa_tools/encrypt_decrypt'
import create_sign from './views/qa_tools/create_sign'
import category from './views/mock/category'
import layout from './components/layout'
import interface2 from './views/mock/interface'
import {Message} from "element-ui";
import page_404 from './views/page_404'
import login from './views/user/login'
import register from './views/user/register'
import calc_game from './views/user/calc_game'
import public_data from './views/mock/public_data'
import mock_index from './views/mock/index'
import user_admin from './views/user/user_admin'
import create_qrcode from './views/qa_tools/create_qrcode'
import person_info from './views/test_data/person_info'
import mock_data from './views/test_data/mock_data'
import VueRouter from "vue-router"
import tools from "./utils/tools"

Vue.use(VueRouter);

const routes = [
    {
        path: '/mock',
        name: "mock",
        redirect: "/mock/index",
        component: layout,
        meta: {"title": "mock平台"},

        children: [
            {
                path: "index",
                component: mock_index,
                name: "mock_index",
                meta: {"title": "说明", "icon": "el-icon-document"}
            },
            {
                path: "public_data",
                component: public_data,
                name: "public_data",
                meta: {"title": "公共参数", "icon": "el-icon-setting"}
            },

            {
                path: "category",
                component: category,
                name: "category",
                meta: {"title": "接口分类", "icon": "el-icon-menu"}
            },
            {
                path: "interface",
                component: interface2,
                name: "interface",
                meta: {"title": "接口管理", "icon": "el-icon-paperclip"}
            },
            {
                path: "baidu",
                name: "baidu",
                meta: {"title": "百度", "icon": "el-icon-paperclip",link:"http://www.baidu.com"}
            },
        ]
    },
    {
        path: '/qa_tools', component: layout, name: "qa_tools",
        meta: {"title": "测试工具"},
        children: [
            {
                path: "",
                component: create_qrcode,
                name: "create_qrcode",
                meta: {"title": "二维码生成", "icon": "el-icon-mobile-phone"}
            },
            {
                path: "encrypt_decrypt",
                component: encrypt_decrypt,
                name: "encrypt_decrypt",
                meta: {"title": "数据加解密", "icon": "el-icon-lock"}
            },
            {
                path: "create_sign",
                component: create_sign,
                name: "create_sign",
                meta: {"title": "签名计算", "icon": "el-icon-edit"}
            }
        ],
    },
    {
        path: '/data', component: layout,
        meta: {"title": "数据中心"},
        children: [
            {
                path: "",
                component: person_info,
                name: "data",
                meta: {"title": "四要素生成", "icon": "el-icon-s-custom"}
            },
            {
                path: "mock_data",
                component: mock_data,
                name: "data",
                meta: {"title": "接口数据构造", "icon": "el-icon-bank-card"}
            },
        ],
    },
    {
        path: '/user_admin', component: layout,
        children: [{
            path: "",
            component: user_admin,
            name: "user_admin",
            meta: {"title": "新增用户", "icon": "el-icon-setting"}
        }],
        meta: {"title": "用户管理", "roles": ["qa"]}
    },
    {path: '*', name: "page_404", component: page_404, meta: {is_nav: false}},
    {path: '/', name: "index", redirect: "/mock", meta: {is_nav: false}},
    {path: '/login', name: "login", component: login, meta: {is_nav: false}},
    {path: '/calc_game', name: "calc_game", component: calc_game, meta: {is_nav: false}},
    {path: '/register', name: "register", component: register, meta: {is_nav: false}},
    {path: '/doc', name: "apidoc", meta: {title: "接口文档",link:"http://sky.nnzhp.cn/doc/index.html"}},
]

var router = new VueRouter({routes})

router.beforeEach((to, from, next) => {
    if (to.path !== "/login" && to.path !== "/register" && to.path !== "/calc_game") {
        var token = tools.get_token()
        if (token) {
            next()
        } else {
            Message({
                message: "token失效，请重新登录！",
                type: 'error',
                duration: 5 * 1000
            })
            router.push('/login')
        }
    } else {
        next()
    }


})
export default router