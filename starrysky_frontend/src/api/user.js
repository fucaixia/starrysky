import request from "../utils/request";

export function login(data) {
    return request({
        url: "/user/login",
        method: "post",
        data
    })
}

export function get_user_info() {
    return request({
        url: "/user/user_info",
        method: "get"
    })
}

export function logout() {
    return request({
        url: "/user/logout",
        method: "post"
    })
}

export function register(data) {

    var formData = new FormData();
    for (var key in data) {
        formData.append(key, data[key])
    }
    return request({
        url: "/user/register",
        method: "post",
        data: formData
    })

}

export function change_user_info(data) {

    var formData = new FormData();
    for (var key in data) {
        formData.append(key, data[key])
    }
    return request({
        url: "/user/user_change",
        method: "post",
        data: formData
    })

}

export function change_password(data) {
    return request({
        url: "/user/change_password",
        method: "post",
        data
    })
}


export function calc_game(data) {
    return request({
        url: "/user/calc_game",
        method: "get",
        params: data
    })
}
