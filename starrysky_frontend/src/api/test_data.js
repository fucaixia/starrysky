import request from "../utils/request";

export function person_info(data) {
    return request({
        url: "/qa_tools/person_info",
        method: "get",
        params: data
    })
}

export function interface_mock(data) {
    return request({
        url: "/qa_tools/interface_mock",
        method: "get",
        params: data

    })
}