from django.db import models

# Create your models here.

class BaseModel(models.Model):
    #这个表是用来继承的，因为创建时间和修改时间每个表里面都有
    update_time = models.DateTimeField(auto_now=True,verbose_name="更新时间")
    create_time = models.DateTimeField(auto_now=True,verbose_name="创建时间")

    class Meta:
        abstract = True

class Author(BaseModel):
    real_name = models.CharField(verbose_name="作者姓名",max_length=50)
    phone = models.CharField(verbose_name="手机号",max_length=11,unique=True)

    class Meta:
        db_table = "author"
        verbose_name = "作者"
        verbose_name_plural = verbose_name  #在django admin里面这个表的名字
        ordering = ["-update_time"] #排序

    def __str__(self):
        return self.real_name

class Book(BaseModel):
    name = models.CharField(verbose_name="书名",max_length=50)
    price = models.IntegerField(verbose_name="价格",default=500)
    count = models.IntegerField(verbose_name="数量",default=100)
    author = models.ForeignKey(Author,on_delete=models.SET(1),verbose_name="作者")

    class Meta:
        db_table = "book"
        verbose_name = "书籍"
        verbose_name_plural = verbose_name  #在django admin里面这个表的名字
        ordering = ["-update_time"] #排序

    def __str__(self):
        return self.name

    #什么也不做 models.DO_NOTHING
    #受保护的 models.PROTECT
    #删除 models.CASCADE
    #设置默认 models.SET_DEFAULT
    #设置成空值 models.SET_NULL
    #设置成指定的 models.SET

#当前图书的总价值
#某个图书的总价格