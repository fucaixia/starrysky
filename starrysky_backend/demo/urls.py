from django.urls import path
from . import views
urlpatterns = [
    path('author', views.AuthorView.as_view() ),
    path('book', views.BookView.as_view() ),
    path('book_value', views.BookValue.as_view() ),
]