from django.db import models
from common.custom_class import BaseModel
from user.models import User


# Create your models here.

class Project(BaseModel):
    name = models.CharField(verbose_name="项目名称", max_length=50)
    git_url = models.CharField(verbose_name="git仓库地址", max_length=100, null=True, blank=True)
    git_branch = models.CharField(verbose_name="git分支", max_length=50, null=True, blank=True)
    py_config = models.TextField(verbose_name="项目配置", null=True, blank=True)
    create_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_cuser")
    update_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_muser")

    class Meta:
        verbose_name = "项目"
        verbose_name_plural = verbose_name
        ordering = ['-id']

    def __str__(self):
        return self.name


class Interface(BaseModel):
    name = models.CharField(verbose_name="接口文件名称", max_length=50)
    code = models.TextField(verbose_name="接口代码")
    project = models.ForeignKey(Project, on_delete=models.PROTECT, verbose_name="归属项目")
    create_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_cuser")
    update_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_fmuser")

    class Meta:
        verbose_name = "接口"
        verbose_name_plural = verbose_name
        unique_together = ["name", "project"]

    def __str__(self):
        return self.name


class Fixture(BaseModel):
    name = models.CharField(verbose_name="fixture名称", max_length=50, unique=True)
    code = models.TextField(verbose_name="fixture代码")
    create_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_fcuser")
    update_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_fmuser")

    class Meta:
        verbose_name = "Fixture"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class Report(BaseModel):
    pass



class Case(BaseModel):
    name = models.CharField(verbose_name="用例名称", max_length=50, unique=True)
    desc = models.CharField(verbose_name="用例描述", max_length=100, null=True, blank=True)
    code = models.TextField(verbose_name="case代码")
    project = models.ForeignKey(Project, verbose_name="项目", on_delete=models.PROTECT, related_name="firefly_project")
    status = models.CharField(max_length=20,null=True, blank=True, verbose_name="用例状态")
    run_time = models.DateTimeField(verbose_name="运行时间", null=True, blank=True)
    duration_time = models.IntegerField(verbose_name="运行时长", null=True, blank=True, default=0)
    run_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_case_run_user",
                                 verbose_name="运行用户")
    create_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_case_cuser")
    update_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_case_uuser")

