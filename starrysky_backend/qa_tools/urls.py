from django.urls import path
from . import views

urlpatterns = [
    path('encrypt_decrypt', views.InterfaceEncryptDecrypt.as_view()),
    path('create_sign', views.CreateSignView.as_view()),
    path('person_info', views.PersonView.as_view()),
    path('interface_mock', views.InterfaceMockView.as_view()),
    path('kabin_info', views.get_kabin),
]
