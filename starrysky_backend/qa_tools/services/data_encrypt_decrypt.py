from nn_rsa import NnRsa

from common.log import Log
from qa_tools import const


def encrypt_decrypt(type, data):
    Log.info("加密/解密前的字符串:{}", data)
    rsa_obj = NnRsa(public_key=const.public_key, private_key=const.private_key)
    if type == "1":
        message = rsa_obj.encrypt(data)
        Log.info("加密后的字符串:{}", message)
        return message

    ret = rsa_obj.decrypt(data)
    if ret:
        Log.info("解密后的字符串:{}", ret)
        return ret
    Log.warning("解密失败！data:{}", data)
    return "解密失败，请检查密文是否正确！"
