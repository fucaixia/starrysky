from common.utils import md5


def create_sign(data: dict):
    data_list = []
    keys = list(data.keys())
    keys.sort()
    for key in keys:
        if key == "sign":
            continue
        value = data.get(key)
        item = "%s=%s" %(key,value)
        data_list.append(item)
    msg = "&".join(data_list)
    return md5(msg)

