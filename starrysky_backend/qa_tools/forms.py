from chameleon.models import Interface
from django import forms
from django.core.exceptions import ValidationError

from common.custom_form import FlyCForm
from .services.person_bank import BankCard


class InterfaceEncryptDecryptForm(FlyCForm):
    type = forms.ChoiceField(choices=((1, "加密"), (2, "解密")))
    # 1加密 2解密
    data = forms.CharField()


class CreateSignForm(FlyCForm):
    data = forms.JSONField()


class PersonForm(FlyCForm):
    bank_code = forms.CharField()
    card_type = forms.ChoiceField(choices=(("DC", "借记卡"), ("CC", "信用卡")))

    def clean_bank_code(self):
        bank_code = self.cleaned_data["bank_code"]
        if bank_code not in BankCard.get_kabin_info():
            raise ValidationError("银行不存在！")
        return bank_code

    def clean(self):
        cleaned_data = super().clean()

        if not self.errors:
            bank_code = cleaned_data["bank_code"]
            card_type = cleaned_data["card_type"]
            bank_kabin = BankCard.get_kabin_info().get(bank_code)
            if card_type not in bank_kabin:
                raise ValidationError("该银行下没有此类型的银行卡")
            return cleaned_data


class InterfaceMockForm(FlyCForm):
    limit = forms.IntegerField()
    name_prefix = forms.CharField()
    url_prefix = forms.CharField()

    def clean_url_prefix(self):
        url_prefix = self.cleaned_data["url_prefix"]
        if Interface.objects.filter(url__contains=url_prefix).exists():
            raise ValidationError("相同开头的url已经存在！")
        if url_prefix.startswith('/'):
            url_prefix = url_prefix.strip('/')
        return url_prefix
