from django.apps import AppConfig


class QaToolsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'qa_tools'
