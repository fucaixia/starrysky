from django.views import View

from . import forms
from . import models
from common.custom_views import FlyView, BaseView, GetView, PutView, PostView
from common.custom_response import FlyResponse


# Create your views here.
class PublicParamView(BaseView, GetView, PostView, PutView):
    form_class = forms.PublicParamForm
    model_class = models.PublicParam

    def __init__(self, *args, **kwargs):
        super(PublicParamView, self).__init__(*args, **kwargs)

    # get
    """
    @api {get} /api/chameleon/public_param 1、获取公共代码
    @apiGroup 变色龙
    @apiUse TokenHeader
    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {List} data  返回数据
    @apiSuccess (返回参数) {Number} data.id  参数id
    @apiSuccess (返回参数) {String} data.code  自定义代码
    @apiSuccess (返回参数) {json} data.create_user  创建用户，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {json} data.update_user  修改用户，，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {String} data.create_time  创建时间
    @apiSuccess (返回参数) {String} data.update_time  修改时间

    @apiSuccessExample {json} 返回报文
        {
            "code": 0,
            "msg": "操作成功",
            "data": [
            {
            "id": 2,
            "create_time": "2021-08-19 20:01:19",
            "update_time": "2021-09-01 16:11:45",
            "code": "token = \"123566788\"",
            "create_user": {
            "id": 1,
            "create_time": "2021-08-18 14:01:49",
            "update_time": "2021-08-20 23:42:46",
            "phone": "18612532945",
            "email": "admin@qq.com",
            "password": "7da387a4cf65ab80356f8e4e38f1c290",
            "nick": "水壶",
            "avatar": "static/avatar/default.jpg",
            "roles": [
            {
            "id": 3,
            "create_time": "2021-08-20 23:40:55",
            "update_time": "2021-08-20 23:40:55",
            "name": "管理员",
            "alias": "admin"
            }
            ],
            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/default.jpg"
            },
            "update_user": {
            "id": 20,
            "create_time": "2021-09-01 15:28:46",
            "update_time": "2021-09-01 15:28:46",
            "phone": "13462245021",
            "email": "763874651@qq.com",
            "password": "b624e6b83fb16c6b0daf493af64e389f",
            "nick": "tony",
            "avatar": "static/avatar/default.jpg",
            "roles": [],
            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/default.jpg"
            }
            }
            ],
            "count": 1
    }

    """

    def post(self, request):
        """
        @api {post} /api/chameleon/public_param 2、提交公共代码
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  [id]          id，修改时必传
        @apiParam {String}  code          自定义代码
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id

        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }

        """
        if self.model_class.objects.filter().exists():
            self.request.PUT = self.request.POST
            return self.put(request)
        return super().post(request)


class InterfaceCategoryView(FlyView):
    search_field = ["name", "prefix"]
    form_class = forms.InterfaceCategoryForm
    model_class = models.InterfaceCategory

    # get
    """
    @api {get} /api/chameleon/interface_category 3、获取接口分类列表
    @apiGroup 变色龙
    @apiUse TokenHeader
    @apiParam {Number}  limit=20        每页数量
    @apiParam {Number}  page=1          第几页
    @apiParam {String}  [search]        模糊搜索
    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {Number} count  总数量
    @apiSuccess (返回参数) {List} data  返回数据
    @apiSuccess (返回参数) {Number} data.id  分类id
    @apiSuccess (返回参数) {String} data.name  分类名称，唯一
    @apiSuccess (返回参数) {String} data.prefix  分类前缀，唯一
    @apiSuccess (返回参数) {json} data.create_user  创建用户，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {json} data.update_user  修改用户，，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {String} data.create_time  创建时间
    @apiSuccess (返回参数) {String} data.update_time  修改时间
    @apiSuccessExample {json} 返回报文
        {
	"code": 0,
	"msg": "操作成功",
	"data": [{
		"id": 5,
		"create_time": "2021-08-31 20:08:38",
		"update_time": "2021-08-31 20:08:38",
		"name": "订单中心",
		"prefix": "order",
		"create_user": {
			"id": 14,
			"create_time": "2021-08-27 22:20:27",
			"update_time": "2021-08-27 22:20:27",
			"phone": "13812531232",
			"email": "admin1@qq.com",
			"password": "7da387a4cf65ab80356f8e4e38f1c290",
			"nick": "你是猪",
			"avatar": "static/avatar/aaa.jpeg",
			"roles": [],
			"full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
		},
		"update_user": {
            "id": 14,
            "create_time": "2021-08-27 22:20:27",
            "update_time": "2021-08-27 22:20:27",
            "phone": "13812531232",
            "email": "admin1@qq.com",
            "password": "7da387a4cf65ab80356f8e4e38f1c290",
            "nick": "你是猪",
            "avatar": "static/avatar/aaa.jpeg",
            "roles": [],
            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
            }
	}],
	"count": 1
}

    """
    #post
    """
        @api {post} /api/chameleon/interface_category 4、新增接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    #put
    """
        @api {put} /api/chameleon/interface_category 5、修改接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id，修改时必传
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    #delete
    """
        @api {delete} /api/chameleon/interface_category 6、删除接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """



class InterfaceView(FlyView):
    search_field = ["name", "desc", "url", "response"]
    filter_field = ["category"]
    form_class = forms.InterfaceForm
    model_class = models.Interface

    # get
    """
    @api {get} /api/chameleon/interface 7、获取接口列表
    @apiGroup 变色龙
    @apiUse TokenHeader
    @apiParam {Number}  limit=20        每页数量
    @apiParam {Number}  page=1          第几页
    @apiParam {String}  [search]        模糊搜索
    @apiParam {String}  [category]      分类id，根据分类筛选
    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {Number} count  总数量
    @apiSuccess (返回参数) {List} data  返回数据
    @apiSuccess (返回参数) {Number} data.id  接口id
    @apiSuccess (返回参数) {String} data.name  接口名称，唯一
    @apiSuccess (返回参数) {String} data.desc  接口描述
    @apiSuccess (返回参数) {Json} data.category  接口归属分类，具体字段见接口分类返回结果
    @apiSuccess (返回参数) {String} data.url  接口url
    @apiSuccess (返回参数) {Json} data.method  接口请求方式
    @apiSuccess (返回参数) {String} data.method.name  接口请求方式名称
    @apiSuccess (返回参数) {Number} data.method.value  接口请求方式id
    @apiSuccess (返回参数) {String} data.response  接口返回结果
    @apiSuccess (返回参数) {String} data.response_header  接口响应头
    @apiSuccess (返回参数) {String} data.code  接口自定义代码
    @apiSuccess (返回参数) {String} data.proxy_url  转发url
    @apiSuccess (返回参数) {Boolean} data.proxy_tag  是否转发开关
    @apiSuccess (返回参数) {Boolean} data.status  接口状态
    @apiSuccess (返回参数) {json} data.create_user  创建用户，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {json} data.update_user  修改用户，，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {String} data.create_time  创建时间
    @apiSuccess (返回参数) {String} data.update_time  修改时间
    @apiSuccessExample {json} 返回报文
            {
                "code": 0,
                "msg": "操作成功",
                "data": [{
                        "id": 41,
                        "create_time": "2021-08-31 16:53:28",
                        "update_time": "2021-08-31 16:55:57",
                        "name": "fmz_fcx_0",
                        "desc": null,
                        "url": "/fcx/interface_0",
                        "category": {
                            "id": 1,
                            "create_time": "2021-08-19 00:24:50",
                            "update_time": "2021-08-19 00:24:53",
                            "name": "用户中心",
                            "prefix": "usercenter",
                            "create_user": {
                                "id": 1,
                                "create_time": "2021-08-18 14:01:49",
                                "update_time": "2021-08-20 23:42:46",
                                "phone": "18612532945",
                                "email": "admin@qq.com",
                                "password": "7da387a4cf65ab80356f8e4e38f1c290",
                                "nick": "水壶",
                                "avatar": "static/avatar/default.jpg",
                                "roles": [{
                                    "id": 3,
                                    "create_time": "2021-08-20 23:40:55",
                                    "update_time": "2021-08-20 23:40:55",
                                    "name": "管理员",
                                    "alias": "admin"
                                }],
                                "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/default.jpg"
                            },
                            "update_user": {
                                "id": 1,
                                "create_time": "2021-08-18 14:01:49",
                                "update_time": "2021-08-20 23:42:46",
                                "phone": "18612532945",
                                "email": "admin@qq.com",
                                "password": "7da387a4cf65ab80356f8e4e38f1c290",
                                "nick": "水壶",
                                "avatar": "static/avatar/default.jpg",
                                "roles": [{
                                    "id": 3,
                                    "create_time": "2021-08-20 23:40:55",
                                    "update_time": "2021-08-20 23:40:55",
                                    "name": "管理员",
                                    "alias": "admin"
                                }],
                                "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/default.jpg"
                            }
                        },
                        "method": {
                            "name": "post",
                            "value": 1
                        },
                        "response": "mock接口_fmz_0",
                        "response_header": "{}",
                        "code": "",
                        "proxy_url": null,
                        "proxy_tag": false,
                        "status": true,
                        "create_user": {
                            "id": 2,
                            "create_time": "2021-08-18 14:29:52",
                            "update_time": "2021-08-30 21:26:37",
                            "phone": "13810631245",
                            "email": "test1@qq.com",
                            "password": "7da387a4cf65ab80356f8e4e38f1c290",
                            "nick": "水果",
                            "avatar": "static/avatar/sgbg_83hH1ex.jpeg",
                            "roles": [{
                                "id": 3,
                                "create_time": "2021-08-20 23:40:55",
                                "update_time": "2021-08-20 23:40:55",
                                "name": "管理员",
                                "alias": "admin"
                            }],
                            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/sgbg_83hH1ex.jpeg"
                        },
                        "update_user": {
                            "id": 3,
                            "create_time": "2021-08-18 14:31:47",
                            "update_time": "2021-08-20 23:42:58",
                            "phone": "13810631241",
                            "email": "test2@qq.com",
                            "password": "7da387a4cf65ab80356f8e4e38f1c290",
                            "nick": "风扇",
                            "avatar": "static/avatar/default.jpg",
                            "roles": [{
                                "id": 3,
                                "create_time": "2021-08-20 23:40:55",
                                "update_time": "2021-08-20 23:40:55",
                                "name": "管理员",
                                "alias": "admin"
                            }],
                            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/default.jpg"
                        }
                    }
                }],
            "count": 1
            }

    """
    # post
    """
        @api {post} /api/chameleon/interface 8、新增接口
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {String}  name          接口名称
        @apiParam  {String} name  接口名称
        @apiParam  {String} [desc]  接口描述
        @apiParam  {Number} category  接口归属分类id
        @apiParam  {String} url  接口url
        @apiParam  {Number} method  接口请求方式id 0 get，1 post，2 put ，3 delete
        @apiParam  {String} response  接口返回结果
        @apiParam  {String} response_header={}  接口响应头
        @apiParam  {String} [code]  接口自定义代码
        @apiParam  {String} [proxy_url]  转发url,转发开关为开时必填
        @apiParam  {Boolean} proxy_tag=false  是否转发开关
        @apiParam  {Boolean} status=true  接口状态
        @apiParam  {Number} create_user  创建用户id
        @apiParam  {Number} update_user  修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # put
    """
        @api {put} /api/chameleon/interface 9、修改接口
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {String}  name          接口名称
        @apiParam  {Number} id  接口id
        @apiParam  {String} name  接口名称
        @apiParam  {String} [desc]  接口描述
        @apiParam  {Number} category  接口归属分类id
        @apiParam  {String} url  接口url
        @apiParam  {Number} method  接口请求方式id 0 get，1 post，2 put ，3 delete
        @apiParam  {String} response  接口返回结果
        @apiParam  {String} response_header={}  接口响应头
        @apiParam  {String} [code]  接口自定义代码
        @apiParam  {String} [proxy_url]  转发url,转发开关为开时必填
        @apiParam  {Bool} proxy_tag=false  是否转发开关
        @apiParam  {Bool} status=true  接口状态
        @apiParam  {Number} create_user  创建用户id
        @apiParam  {Number} update_user  修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # delete
    """
        @api {delete} /api/chameleon/interface 10、删除接口
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """


class InterfaceBatchDeleteView(View):
    def post(self, request):
        """
            @api {post} /api/chameleon/interface_batch_delete 11、批量删除接口
            @apiGroup 变色龙
            @apiUse TokenHeader
            @apiParam {List}  ids          要删除的id
            @apiSuccess (返回参数) {int} code  状态码
            @apiSuccess (返回参数) {String} msg  错误信息
            @apiSuccessExample {json} 返回报文
                {
                    "code":0,
                    "msg":"操作成功"
                }
            """
        form = forms.InterfaceDeleteForm(request.POST)
        if form.is_valid():
            ids = form.cleaned_data["ids"]
            models.Interface.objects.filter(id__in=ids).delete()
            return FlyResponse()
        return FlyResponse(-1, msg=form.error_msg)
