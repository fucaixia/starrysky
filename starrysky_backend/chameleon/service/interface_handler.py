import re, json
from proxy.views import proxy_view
from chameleon import models
from .execute_code import exec_code
from django.http.response import HttpResponse
from common.log import Log


class InterfaceHandler:

    def __init__(self, category: models.InterfaceCategory, request):
        self.category = category
        self.request = request
        self.interface = None

    def handle_code(self):
        public_code = models.PublicParam.objects.all().first().code
        public_param = exec_code(public_code)  # 执行公共代码
        interface_param = exec_code(self.interface.code)
        public_param.update(interface_param)
        return public_param

    @property
    def replace_fields(self):  # 获取所有需要替换的字段
        fields = re.findall('\$\{(.*?)\}', self.interface.response)
        return fields

    def replace_response(self):
        if self.replace_fields:
            code_data = self.handle_code()
            request_data = self.handle_request_param()
            # request_data.update(code_data)
            code_data.update(request_data)
            for field in self.replace_fields:
                value = code_data.get(field)
                if value != None:
                    self.interface.response = self.interface.response.replace("${%s}" % field, str(value))

    def handle_request_param(self):
        request_data = {}
        for k in self.request.GET:
            request_data[k] = self.request.GET.get(k)
        if self.request.method.upper() not in ["GET", "DELETE"]:
            request_post = getattr(self.request, self.request.method.upper())
            for k in request_post:
                request_data[k] = request_post.get(k)

        return request_data

    def handle_proxy(self):
        if bool(self.interface.proxy_url):
            try:
                response = proxy_view(self.request, self.interface.proxy_url)
            except:
                Log.info("转发失败，url : %s" % self.interface.proxy_url)
                return '{"code":-1,"error_msg":"转发失败，"url" : "%s"}' % self.interface.proxy_url
            return response
        else:
            return '转发url为空，无法转发！'

    def set_response_header(self):
        if self.interface.response_header:
            try:
                response_header = json.loads(self.interface.response_header)
            except:
                Log.info("响应头设置出错，json格式不合法！")
            else:
                for k, v in response_header.items():
                    self.response[k] = v

    def handler(self):  # /tqz/weather/add
        uri = '/' + '/'.join(self.request.path_info.split('/')[2:])
        interface_filter = self.category.interface_set.filter(url=uri)
        if not interface_filter:
            return HttpResponse('{"code":-1,"error_msg":"接口%s不存在！"}' % uri)

        self.interface = interface_filter.first()

        if not self.interface.status:
            return HttpResponse('{"code":-1,"error_msg":"接口未完成"}')

        if self.interface.proxy_tag:  # 判断转发开关
            return HttpResponse(self.handle_proxy())

        self.replace_response()
        self.response = HttpResponse(self.interface.response)
        self.set_response_header()
        return self.response
