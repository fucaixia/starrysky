from django.middleware.common import MiddlewareMixin
from . import models
from .service import interface_handler


class ChameleonMiddleWare(MiddlewareMixin):

    @staticmethod
    def process_request(request):
        category_prefix = request.path_info.split('/')[1]  # /tqz/weather
        interface_category = models.InterfaceCategory.objects.filter(prefix=category_prefix)
        if interface_category:
            handler = interface_handler.InterfaceHandler(interface_category.first(), request)
            ret = handler.handler()
            return ret
