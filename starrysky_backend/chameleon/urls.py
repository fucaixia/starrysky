from django.urls import path
from . import views

urlpatterns = [
    path('interface', views.InterfaceView.as_view()),
    path('interface_batch_delete', views.InterfaceBatchDeleteView.as_view()),
    path('public_param', views.PublicParamView.as_view()),
    path('interface_category', views.InterfaceCategoryView.as_view()),
]
