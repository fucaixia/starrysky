from django.test import TestCase
import re

# Create your tests here.
s = '''
import time
cur_time = int(time.time())
score = 38
'''
#    ${.*?}
exec(s)
print(locals())


class MyClass:
    def __init__(self):
        self.new_func = self.my_func
        self.my_func = None
        del self.my_func

    def my_func(self):
        print("func..")


if __name__ == '__main__':
    m = MyClass()
    m.new_func()
