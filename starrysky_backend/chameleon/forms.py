from django.core.exceptions import ValidationError

from common.custom_form import FlyForm, FlyCForm
from . import models
from django import forms
import json


class PublicParamForm(FlyForm):
    class Meta:
        model = models.PublicParam
        fields = '__all__'


class InterfaceCategoryForm(FlyForm):
    class Meta:
        model = models.InterfaceCategory
        fields = '__all__'


class InterfaceForm(FlyForm):
    class Meta:
        model = models.Interface
        fields = '__all__'


class InterfaceDeleteForm(FlyCForm):
    ids = forms.JSONField()

    def clean_ids(self):
        ids = self.cleaned_data["ids"]
        if type(ids) != list:
            raise ValidationError("ids必须是一个list")
        return ids
