"""
ASGI config for starrysky_backend project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter
from starrysky_backend import routings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'starrysky_backend.settings')

application = ProtocolTypeRouter(
    {
        "http":get_asgi_application(),#处理http请求的，他会自动找到urls去映射view
        "websocket":URLRouter(routings.websocket_urlpatterns) #这个是处理websocket请求的
    }
)
