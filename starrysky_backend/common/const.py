import os, platform

base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

page_limit = 50
# 是否开启debug日志
is_local = False if platform.system() == "Linux" else True

# 日志文件配置
if is_local:
    sky_log_path = os.path.join(base_dir, 'logs', 'sky.log')
    db_info = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(base_dir, 'db.sqlite3'),

    }

else:
    sky_log_path = os.path.join('/home/jenkins/work', 'logs', 'sky.log')
    db_host = "127.0.0.1"
    db_info = {
        'ENGINE': 'django.db.backends.mysql',
        'USER': "sky",  # 用户
        "HOST": db_host,
        "PASSWORD": "sky123456",
        "PORT": 3306,
        "NAME": "sky"  # 数据库
    }

port = 8777
secret_key = "1234A#CD"
