from loguru import logger
from .const import sky_log_path


class SkyLog:

    @staticmethod
    def get_logger(log_path=sky_log_path):
        class ChildLog:
            logger.add(log_path, level="DEBUG", encoding='utf-8', enqueue=True, rotation='1 day',
                       retention='10 days')  # 写在日志文件里面

            debug = logger.debug
            info = logger.info
            warning = logger.warning
            error = logger.error
            catch = logger.catch

        return ChildLog


Log = SkyLog.get_logger()
