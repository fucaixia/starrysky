from nn_rsa import NnRsa
from qa_tools import const
from qa_tools.services import data_sign
from common.custom_response import FlyResponse
from common.error_code import SkyErrorCode
from common.log import Log
import json
from user.service.user_service import UserService


def check_token(func):
    def inner(*args, **kwargs):
        request = args[1]
        header_token = request.META.get("HTTP_TOKEN")  # header获取token
        cookie_token = request.COOKIES.get("token")
        param_token = request.GET.get("token")
        token = header_token if header_token else cookie_token
        if param_token:
            token = param_token
        if not token:
            return FlyResponse(SkyErrorCode.NOT_LOGIN, msg="请登录！")
        user_service = UserService()
        user = user_service.token_to_user_info(token)  # 1、校验token是否存在 2、获取到用户信息
        if not user:
            Log.warning("缓存中获取不到token信息")
            return FlyResponse(SkyErrorCode.NOT_LOGIN, msg="请登录！")
        request.user = user
        request.token = token
        Log.debug("当前用户:{},{},{}", user.nick, user.phone, user.email)
        return func(*args, **kwargs)

    return inner


def data_encrypt(func):
    def inner(*args, **kwargs):
        request = args[1]
        req_data = request.POST.get("data")
        if not req_data:
            return FlyResponse(SkyErrorCode.ERROR, msg="data不能为空")
        rsa_obj = NnRsa(public_key=const.public_key, private_key=const.private_key)
        ret = rsa_obj.decrypt(req_data)
        if ret == False:
            return FlyResponse(SkyErrorCode.ERROR, msg="data解密失败！")
        try:
            request.POST = json.loads(ret)
        except:
            return FlyResponse(SkyErrorCode.ERROR, msg="data不正确，解密后的结果不是一个合法的json！")
        ret = func(args[0], request)
        response_data = rsa_obj.encrypt(ret.content.decode())
        return FlyResponse(data=response_data)

    return inner


def check_sign(func):
    def inner(*args, **kwargs):
        request = args[1]
        sign = request.POST.get("sign")
        if not sign:
            return FlyResponse(SkyErrorCode.ERROR, msg="验签失败！")
        local_sign = data_sign.create_sign(request.POST)
        if local_sign != sign:
            return FlyResponse(SkyErrorCode.ERROR, msg="验签失败！")
        ret = func(args[0], request)
        return ret

    return inner
