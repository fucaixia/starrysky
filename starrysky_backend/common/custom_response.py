# -*- coding:utf-8 -*-
# @FileName  :custom_response.py
# @Time      :2021-06-06 16:12
# @Author    :niuhanyang

from django.http.response import JsonResponse
from .error_code import SkyErrorCode


def FlyResponse(code=SkyErrorCode.SUCCESS, msg="操作成功", cookies=None, headers=None,http_code=200, **kwargs):
    data = {"code": code, "msg": msg}
    data.update(kwargs)
    ret = JsonResponse(data, json_dumps_params={"ensure_ascii": False})
    ret.status_code = http_code
    if cookies:
        for item, value in cookies.items():
            ret.set_cookie(item, value)
    if headers:
        for item, value in headers.items():
            ret[item] = value

    return ret
