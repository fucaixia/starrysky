from django.views import View
from .custom_response import FlyResponse
from django.db.models import Q
from django.core.paginator import Paginator
from . import const
from .utils import model_to_dict


class BaseView(View):
    form_class = None
    model_class = None
    filter_field = ()
    search_field = ()
    exclude_field = ()
    limit = const.page_limit
    db_instance = None

    def get_page_data(self):
        query_set = self.get_filter_query_set()  # 获取筛选的结果
        q_obj = self.get_search_obj()  # 获取模糊查询的条件
        data = query_set.filter(q_obj)
        limit, page = self.check_page_param()  # 获取分页limit和页数
        paginator = Paginator(data, limit)
        page_data = paginator.page(page)
        return page_data, paginator.count

    def get_filter_query_dict(self):
        filter_dict = {}  # {"teacher":None}
        for field in self.filter_field:
            value = self.request.GET.get(field)
            if value:
                filter_dict[field] = value
        return filter_dict

    def get_filter_query_set(self):
        filter_dict = self.get_filter_query_dict()
        return self.model_class.objects.filter(**filter_dict)

    def get_search_obj(self):
        # search_field = ["teacher"，"name"]
        q_obj = Q()  # 空白的q对象
        search = self.request.GET.get('search')  # search=小黑
        if search:
            for field in self.search_field:
                d = {'%s__contains' % field: search}  # {"teacher_contains":小黑} ,{"name_contains":小黑}
                q_obj = q_obj | Q(**d)  # 这是每次在拼Q对象，生成or条件# Q(teacher_contains=小黑)
        return q_obj

    def check_page_param(self):
        limit = str(self.request.GET.get("limit", self.limit))
        page = str(self.request.GET.get("page", 1))

        if limit.isdigit() and page.isdigit() and int(limit) > 0 and int(page) > 0:
            return limit, page


class PostView:
    def post(self, request):
        form = self.form_class(request.POST, files=request.FILES)
        if form.is_valid():
            obj = form.save()  # 直接能保存到数据库
            self.db_instance = obj
            return FlyResponse()
        return FlyResponse(code=-1, msg=form.error_msg)


class GetView:

    def get(self, request):
        if not self.check_page_param():
            return FlyResponse(-1, "分页信息不正确")
        data_list = []
        page_data, page_count = self.get_page_data()
        for row_obj in page_data:  # 表里面的每一行数据
            row_dict = model_to_dict(row_obj, exclude=self.exclude_field)
            data_list.append(row_dict)
        return FlyResponse(data=data_list, count=page_count)


class PutView:
    def put(self, request):
        id = request.PUT.get("id")
        if not id:
            return FlyResponse(-1, "请传入要修改的id")

        obj = self.model_class.objects.filter(id=id)
        self.db_instance = obj
        if not obj.exists():
            return FlyResponse(-1, "id不存在")

        form = self.form_class(request.PUT, instance=obj.first(), files=request.FILES)

        if form.is_valid():
            form.save()
            return FlyResponse()

        return FlyResponse(code=-1, msg=form.error_msg)


class DeleteView:
    def delete(self, request):
        id = request.GET.get("id")
        if not id:
            return FlyResponse(-1, "请传入要删除的id")
        obj = self.model_class.objects.filter(id=id)
        self.db_instance = obj
        if not obj.exists():
            return FlyResponse(-1, "删除的id不存在")

        obj.delete()
        return FlyResponse()


class FlyView(BaseView, PostView, DeleteView, PutView, GetView):
    pass
