from django.db import models


class BaseModel(models.Model):
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="更新时间", auto_now=True)

    class Meta:
        abstract = True


class SkyDict(dict):

    def __getattr__(self, item):
        value = self.get(item)
        if type(value) == dict:
            value = self.__class__(value)
        if isinstance(value, list) or isinstance(value, tuple):
            value = list(value)
            for index, v in enumerate(value):
                if isinstance(v, dict):
                    value[index] = self.__class__(v)
        return value


if __name__ == '__main__':
    d = SkyDict(name=1, info=[{"a": "!!!!!!1"}])
    print(d.info[0].a)
