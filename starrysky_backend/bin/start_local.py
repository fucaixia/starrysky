import os
from django.core.management import execute_from_command_line

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'starrysky_backend.settings')
execute_from_command_line([__file__, "runserver", "127.0.0.1:8777"])


