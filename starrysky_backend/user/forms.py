from common.custom_form import FlyForm, FlyCForm
from django import forms

from . import models
from django.db.models import Q
from django.forms.forms import ValidationError
from common.utils import md5
from common.log import Log

class UserRegisterForm(FlyForm):  # 注册使用
    password2 = forms.CharField()

    class Meta:
        fields = '__all__'
        exclude = ["roles"]
        model = models.User

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors:
            password = cleaned_data["password"]
            password2 = cleaned_data["password2"]
            if password2 == password:
                cleaned_data.pop("password2")
                cleaned_data["password"] = md5(password)
                return cleaned_data
            raise ValidationError("两次输入密码不一致!")


class UserChangeForm(FlyForm):  # 修改用户信息，不包括密码
    class Meta:
        exclude = ["password","roles"]
        model = models.User


class LoginForm(FlyCForm):
    username = forms.CharField(max_length=30, min_length=5, required=True)
    password = forms.CharField(max_length=30, min_length=5, required=True)

    def clean_username(self):
        username = self.cleaned_data.get("username")
        user = models.User.objects.filter(Q(phone=username) | Q(email=username))
        if user:
            self.user = user.first()
        else:
            raise ValidationError("用户名不存在！")
        return username

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        if not self.errors:
            password = cleaned_data.get("password")
            password_md5 = md5(password)
            if self.user.password != password_md5:
                raise ValidationError("密码错误！")
        return cleaned_data


class ChangePassword(FlyCForm):
    old_password = forms.CharField(min_length=5, max_length=30)
    new_password = forms.CharField(min_length=5, max_length=30)

    def clean_old_password(self):
        old_password = self.cleaned_data["old_password"]
        if self.user.password != md5(old_password):
            raise ValidationError("旧密码错误！")
        return old_password

    def clean_new_password(self):
        new_password = self.cleaned_data["new_password"]
        return md5(new_password)

class CalcGameForm(FlyCForm):
    cards = forms.CharField()
    calc_number = forms.IntegerField()

    def clean_cards(self):
        new_cards = []
        cards = self.cleaned_data["cards"]
        for card in cards.split():
            Log.info("card:{}",card)
            if not card.isdigit():
                raise ValidationError("牌必须是空格隔开的数字！")
            new_cards.append(int(card))
        return new_cards


