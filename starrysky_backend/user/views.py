from common.custom_views import FlyView, BaseView, GetView, PostView, PutView
from common.custom_response import FlyResponse
from common.error_code import SkyErrorCode
from django.views import View
from user import models
from user import forms
from .service.user_service import UserService
from common.utils import model_to_dict
from common.log import Log
from user.service import calc_game_service


# Create your views here.
class RegisterView(BaseView, PostView):
    model_class = models.User
    form_class = forms.UserRegisterForm

    def post(self, request):
        """
        @api {post} /api/user/register 1、注册
        @apiGroup 用户
        @apiParam {String}  phone          用户手机号
        @apiParam {String}  email          用户邮箱
        @apiParam {String}  nick          用户昵称
        @apiParam {String}  password        用户密码
        @apiParam {String}  password2        密码确认
        @apiParam {File}  [avatar]        头像
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccess (返回参数) {String} token  sessionid
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"注册成功！",
                "token":"58bd8c4fe99ed66295b1c1628854864d"
            }

        """
        ret = super().post(request)
        if self.db_instance:
            user_service = UserService(self.db_instance)
            token = user_service.login()
            ret = FlyResponse(msg="注册成功", token=token, cookies={"token": token})
        return ret


class LoginView(View):

    def post(self, request):
        """
        @api {post} /api/user/login 2、登录
        @apiGroup 用户
        @apiParam {String}  username        手机号/邮箱
        @apiParam {String}  password        用户密码
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccess (返回参数) {String} token  sessionid
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"注册成功！",
                "token":"58bd8c4fe99ed66295b1c1628854864d"
            }

        """
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            user_service = UserService(form.user)
            token = user_service.login()
            return FlyResponse(msg="登录成功", token=token, cookies={"token": token})

        return FlyResponse(SkyErrorCode.ERROR, form.error_msg)


class UserInfoView(View):
    def get(self, request):
        """
        @api {get} /api/user/user_info 3、获取用户信息
        @apiGroup 用户
        @apiUse TokenHeader

        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccess (返回参数) {object} data  数据返回
        @apiSuccess (返回参数) {Number} data.id  数据返回
        @apiSuccess (返回参数) {String} data.email  邮箱
        @apiSuccess (返回参数) {String} data.phone  手机号
        @apiSuccess (返回参数) {String} data.nick  昵称
        @apiSuccess (返回参数) {String} data.full_avatar_url  头像url
        @apiSuccess (返回参数) {List} data.roles  角色
        @apiSuccess (返回参数) {String} data.update_time  更新时间
        @apiSuccess (返回参数) {String} data.create_time  创建时间
        @apiSuccessExample {json} 返回报文
        {
            "code": 0,
            "msg": "操作成功",
            "data": {
                "id": 2,
                "create_time": "2021-08-18 14:29:52",
                "update_time": "2021-08-30 21:26:37",
                "phone": "13810631245",
                "email": "test1@qq.com",
                "nick": "水果",
                "roles": ["admin"],
                "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/sgbg_83hH1ex.jpeg"
            }
        }
        """
        user_dict = model_to_dict(request.user, exclude=["password", "avatar"])
        user_dict["roles"] = [role.alias for role in user_dict.roles]

        return FlyResponse(data=user_dict)


class UserChangeView(BaseView):  # 修改用户信息，不包括密码
    form_class = forms.UserChangeForm

    def post(self, request):
        """
        @api {post} /api/user/user_change 4、修改用户信息
        @apiGroup 用户
        @apiUse TokenHeader
        @apiParam {String}  phone          用户手机号
        @apiParam {String}  email          用户邮箱
        @apiParam {String}  nick          用户昵称
        @apiParam {File}  [avatar]        头像

        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }

        """
        form = self.form_class(request.POST, instance=request.user, files=request.FILES)
        if form.is_valid():
            user = form.save()
            user_service = UserService(user)
            user_service.update_user_cache(request.token)
            return FlyResponse()

        return FlyResponse(code=SkyErrorCode.ERROR, msg=form.error_msg)


class UserListView(BaseView, GetView):
    model_class = models.User
    filter_field = ["id", "phone", "email"]


class ChangePassWordView(View):
    def post(self, request):
        """
        @api {post} /api/user/change_password 5、修改密码
        @apiGroup 用户
        @apiUse TokenHeader
        @apiParam {String}  old_password          旧密码
        @apiParam {String}  new_password          新密码
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }

        """
        form = forms.ChangePassword(request.POST)
        form.user = request.user
        if form.is_valid():
            new_password = form.cleaned_data["new_password"]
            request.user.password = new_password
            request.user.save()
            user_service = UserService(request.user)
            user_service.logout(request.token)

            return FlyResponse()

        return FlyResponse(SkyErrorCode.ERROR, form.error_msg)


class LogoutView(View):
    def post(self, request):
        """
        @api {post} /api/user/logout 6、退出登录
        @apiGroup 用户
        @apiUse TokenHeader
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }

        """
        user_service = UserService(request.user)
        user_service.logout(request.token)
        return FlyResponse()


class CalcGameView(View):
    def get(self, request):
        form = forms.CalcGameForm(request.GET)
        if form.is_valid():
            numbers = form.cleaned_data["cards"]
            calc_number = form.cleaned_data["calc_number"]
            calc_game = calc_game_service.CalcGame(numbers, calc_number)
            result = calc_game.get_result()
            return FlyResponse(data=result)
        Log.warning("game form校验失败:{}",form.error_msg)
        return FlyResponse(code=-1, msg=form.error_msg)
