from django.middleware.common import MiddlewareMixin
from . import const
from common.log import Log
from common.error_code import SkyErrorCode
from common.custom_response import FlyResponse
from user.service.user_service import UserService


class TokenMiddleWare(MiddlewareMixin):

    @staticmethod
    def check_not_login(path):  # 判断url是否需要登录
        for url in const.not_login_url:
            if url in path:
                Log.info("url:{},不需要登录", path)
                return True

    def process_request(self, request):
        if not self.check_not_login(request.path_info):
            header_token = request.META.get("HTTP_TOKEN") #header获取token
            cookie_token = request.COOKIES.get("token")
            param_token = request.GET.get("token")
            token = header_token if header_token else cookie_token
            if param_token:
                token = param_token
            Log.debug("header_token:{},cookie_token:{},params_token:{}",header_token,cookie_token,param_token)
            if not token:
                return FlyResponse(SkyErrorCode.NOT_LOGIN, msg="请登录！")
            user_service = UserService()
            user = user_service.token_to_user_info(token) #1、校验token是否存在 2、获取到用户信息
            if not user:
                Log.warning("缓存中获取不到token信息")
                return FlyResponse(SkyErrorCode.NOT_LOGIN, msg="请登录！")
            request.user = user
            request.token = token
            Log.debug("当前用户:{},{},{}",user.nick,user.phone,user.email)

