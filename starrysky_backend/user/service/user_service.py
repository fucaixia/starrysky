import django_redis
import time
from common.utils import md5, FileOperator
from user import const
from common.log import Log
import pickle
import os


class UserService:
    def __init__(self, user=None):
        self.user_session_key = const.user_session_key + user.phone if user else ""
        self.user = user
        self.cache_operator = django_redis.get_redis_connection() if const.use_redis else FileOperator(const.cache_file)
        if not const.use_redis:
            Log.info("pick_user_data,{}", self.cache_operator.pickle_read())

    @classmethod
    def create_token(cls, username):
        return md5(username + str(time.time()))

    def local_login(self):
        if not os.path.exists(const.cache_file):  # 文件不存在，生成token，和用户信息存到文件里面，返回token
            token = self.create_token(self.user.phone)
            user_info_key = const.user_info_key + token
            user_info_cache_data = {self.user_session_key: token, user_info_key: self.user}
            self.cache_operator.pickle_write(user_info_cache_data)
            return token

        user_data = self.cache_operator.pickle_read()  # 如果文件里面存在token，直接返回
        token = user_data.get(self.user_session_key)
        if token:
            return token

        # 如果token不存在，重新生成token
        token = self.create_token(self.user.phone)
        user_info_key = const.user_info_key + token
        user_data[self.user_session_key] = token
        user_data[user_info_key] = self.user
        self.cache_operator.pickle_write(user_data)
        return token

    def login(self):
        if const.use_redis:
            return self.redis_login()
        Log.debug("不使用redis，使用本地文件")
        return self.local_login()

    def redis_login(self):
        token = self.cache_operator.get(self.user_session_key)  # user:session:18235235 -  tokenx
        Log.info("self.cache_operator,{}", self.cache_operator)
        if token:  # 如果token存在
            Log.debug("缓存中存在token，直接返回")
            return token.decode()
        token = self.create_token(self.user.phone)
        Log.debug("生成token,{}", token)
        user_info_key = const.user_info_key + token
        p = self.cache_operator.pipeline()
        p.set(self.user_session_key, token, const.user_session_expire)  # 添加token
        p.set(user_info_key, pickle.dumps(self.user), const.user_session_expire)  # 添加token对应的用户信息
        p.execute()
        Log.debug("token存储redis完成")
        return token

    def token_to_user_info(self, token):
        user_info_key = const.user_info_key + token
        if not const.use_redis:
            user_data = self.cache_operator.pickle_read()
            return user_data.get(user_info_key)

        user_info = self.cache_operator.get(user_info_key)
        if user_info:  # 如果redis获取到数据
            return pickle.loads(user_info)

    def update_user_cache(self, token):
        user_info_key = const.user_info_key + token
        user_pickle = pickle.dumps(self.user)
        if const.use_redis:  # 如果使用redis
            Log.debug("开始更新redis中用户信息")
            ttl = self.cache_operator.ttl(user_info_key)
            self.cache_operator.set(user_info_key, user_pickle, ttl)
            Log.debug("redis用户信息更新完成")
            return

        Log.debug("开始更新本地文件中用户信息")
        user_data = self.cache_operator.pickle_read()
        user_data[user_info_key] = self.user
        self.cache_operator.pickle_write(user_data)
        Log.debug("更新本地文件中用户信息完成")

    def logout(self, token):
        user_info_key = const.user_info_key + token
        if const.use_redis:
            self.cache_operator.delete(self.user_session_key)
            self.cache_operator.delete(user_info_key)
            return

        user_data = self.cache_operator.pickle_read()
        user_data.pop(self.user_session_key)
        user_data.pop(user_info_key)
        self.cache_operator.pickle_write(user_data)
