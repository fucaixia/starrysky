from django.urls import path
from . import views
urlpatterns = [
    path('user_change',views.UserChangeView.as_view() ),
    path('change_password',views.ChangePassWordView.as_view() ),
    path('user_info',views.UserInfoView.as_view() ),
    path('register',views.RegisterView.as_view() ),
    path('login',views.LoginView.as_view() ),
    path('logout',views.LogoutView.as_view() ),
    path('user_list', views.UserListView.as_view()),  # 自己查数据方便的
    path('calc_game', views.CalcGameView.as_view()),  # 自己查数据方便的
]
