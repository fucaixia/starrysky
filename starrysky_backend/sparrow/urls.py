from django.urls import path
from . import views

urlpatterns = [
    path('student', views.StudentView.as_view()),
    path('student_encrypt', views.StudentViewEncrypt.as_view()),
    path('student_sign', views.StudentViewSign.as_view()),
]
