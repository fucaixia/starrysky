from channels.generic.websocket import WebsocketConsumer
from channels.exceptions import StopConsumer


class TestConsumer(WebsocketConsumer):

    def websocket_connect(self, message):
        print("创建连接",message)
        self.accept() #接收客户端连接

    def websocket_receive(self, message):
        #收到客户端发送消息
        print('接收到消息', message)
        self.send('收到了') #发送消息给客户端

        #self.close() #服务端如果要主动断开连接，调用close方法

    def websocket_disconnect(self, message):
        print('客户端断开连接了')
        raise StopConsumer() #断开连接


