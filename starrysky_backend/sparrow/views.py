from common.custom_views import BaseView, GetView, PostView
from common.decorators import check_token, data_encrypt, check_sign
from . import models, forms


# Create your views here.

class StudentView(BaseView, GetView, PostView):
    filter_field = ['name', 'sex', 'age', 'grade', 'phone']
    search_field = ['name', 'addr', 'grade', 'phone']
    form_class = forms.StudentForm
    model_class = models.Student
    # get
    """
    @api {get} /api/sparrow/student 1、获取学生列表
    @apiGroup 麻雀
    @apiParam {Number}  limit=20        每页数量
    @apiParam {Number}  page=1          第几页
    @apiParam {String}  [search]        模糊搜索
    @apiParam {String}  [name]        姓名
    @apiParam {String}  [sex]        性别
    @apiParam {String}  [age]        年龄
    @apiParam {String}  [grade]        班级
    @apiParam {String}  [phone]        手机号
    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {Number} count  总数量
    @apiSuccess (返回参数) {List} data  返回数据
    @apiSuccess (返回参数) {Number} data.id  学生id
    @apiSuccess (返回参数) {String} data.name  学生名称
    @apiSuccess (返回参数) {Number} data.age  年龄
    @apiSuccess (返回参数) {String} data.addr  地址
    @apiSuccess (返回参数) {String} data.grade  班级
    @apiSuccess (返回参数) {String} data.phone  手机号
    @apiSuccess (返回参数) {Number} data.gold  金币
    @apiSuccess (返回参数) {String} data.create_time  创建时间
    @apiSuccess (返回参数) {String} data.update_time  修改时间
    @apiSuccessExample {json} 返回报文
    {
            "code": 0,
            "msg": "操作成功",
            "data": [
                {
                "id": 7,
                "create_time": "2021-08-25 20:18:44",
                "update_time": "2021-08-25 20:18:44",
                "name": "白菜",
                "sex": "男",
                "age": 18,
                "addr": "北京市昌平区",
                "grade": "天蝎座",
                "phone": "12345678911",
                "gold": 100
                }
            ],
            "count": 1
            }

    """
    # post
    """
        @api {post} /api/sparrow/student 2、新增学生
        @apiGroup 麻雀
        @apiHeader {String} Content-Type=application/json 请求类型
        @apiHeaderExample  {Header} 请求头示例
            "Content-Type: application/json"
        @apiParam {String}  name         姓名
        @apiParam {String}  grade         班级
        @apiParam {Number}  [age=18]         年龄
        @apiParam {String}  [addr=北京市昌平区]    地址
        @apiParam {Number}  [gold=100]     金币
        @apiParam {String}  phone     手机号，唯一，11位
        @apiParam {String}  [sex=男]     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """


class StudentViewEncrypt(BaseView, PostView):
    form_class = forms.StudentForm
    model_class = models.Student

    @check_token
    @data_encrypt
    def post(self, request):
        """
        @api {post} /api/sparrow/student_encrypt 3、新增学生--加密
        @apiGroup 麻雀
        @apiUse TokenHeader
        @apiParam {String}  data    请求参数加密后的密文，请求参数看接口2新增学生接口
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccess (返回参数) {String} data  返回结果，数据是加密的
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功",
                "data":"ekMAeJB6How1fWVs2eF2QQt1Qu3yVPQcUIWxbgsueFyNSVrCYJQymCjuGjoCNZmwuZLF2AlVTQJb0whMAw5hyg=="
            }
            """
        return super().post(request)


class StudentViewSign(BaseView, PostView):
    form_class = forms.StudentForm
    model_class = models.Student

    @check_sign
    def post(self, request):
        """
            @api {post} /api/sparrow/student_sign 4、新增学生--需要签名
            @apiGroup 麻雀
            @apiHeader {String} Content-Type=application/json 请求类型
            @apiHeaderExample  {Header} 请求头示例
                "Content-Type: application/json"
            @apiParam {String}  name         姓名
            @apiParam {String}  grade         班级
            @apiParam {Number}  [age=18]         年龄
            @apiParam {String}  [addr=北京市昌平区]    地址
            @apiParam {Number}  [gold=100]     金币
            @apiParam {String}  phone     手机号，唯一，11位
            @apiParam {String}  [sex=男]     修改用户id
            @apiParam {String}  sign     接口签名
            @apiSuccess (返回参数) {int} code  状态码
            @apiSuccess (返回参数) {String} msg  错误信息
            @apiSuccessExample {json} 返回报文
                {
                    "code":0,
                    "msg":"操作成功"
                }
            """
        return super().post(request)
