from django.core.exceptions import ValidationError

from common.custom_form import FlyForm
from . import models


class StudentForm(FlyForm):
    def clean_phone(self):
        phone = self.cleaned_data["phone"]
        if len(phone) != 11:
            raise ValidationError("手机号长度不正确！")
        return phone

    class Meta:
        fields = '__all__'
        model = models.Student
