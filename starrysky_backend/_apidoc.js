/**
 * @apiDefine TokenHeader
 * @apiHeader {String} token 登录后返回的token
 * @apiHeaderExample  {Header} token示例
 *       "token: 58bd8c4fe99ed66295b1c1628854864d"
 */

/**
 * @apiDefine JsonTokenHeader
 * @apiHeader {String} token 登录后返回的token
 * @apiHeader {String} Content-Type=application/json 请求类型
 * @apiHeaderExample  {Header} 请求头示例
 *       "Content-Type: application/json"
 *       "token: 58bd8c4fe99ed66295b1c1628854864d"
 */




